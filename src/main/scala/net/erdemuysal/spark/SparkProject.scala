package net.erdemuysal.sparkProject;

import org.apache.spark.sql.SparkSession
import org.apache.spark.rdd.RDD
import java.io.File
import scala.collection.mutable.ListBuffer
import java.util.Calendar
import org.apache.spark.sql.Row
import org.apache.spark.rdd.UnionRDD

/**
 * @author Erdem Uysal - 201571007
 */
object SparkProject {
  
  //exaggerated boundary box of newyork. Used for error correction on geographic data 
  val minLong : Double = -80.573730;
  val maxLong : Double = -67.126464;
  val minLat  : Double =  35.995785;
  val maxLat  : Double =  45.305802;
  
  //default zoom level for grouping points as tile squares
  val ZOOM_LEVEL : Integer = 19
  

  /**
   * @param long  longitude or x of geographic coordinate
   * @param lat   latitude or y of geographic coordinate
   * @param zoom  Z tag of requested tile square
   * @return ([tileX], [tileY]) on requested Z level
   */
  def WGSToTile(long: Double, lat: Double, zoom: Integer) : (Int, Int) = {
    val x = (long + 180) / 360;
    val sinLatitude = Math.sin(lat * Math.PI / 180);
    
    val div = (1 + sinLatitude) / (1 - sinLatitude);
    val y = 0.5 - Math.log( div ) / (4 * Math.PI);
    
    val mapSize = 256 << zoom;
    val pixelX = (x * mapSize) + 0.5;
    val pixelY = (y * mapSize) + 0.5;
    
    var tileX : Integer = (pixelX / 256).toInt;
    var tileY : Integer = (pixelY / 256).toInt;
    (tileX, tileY)
  }
  
  /**
   * @param tileX square's X tag
   * @param tileY square's Y tag
   * @param zoom  square's Z tag
   * @return ([long], [lat]) of upperleft location of tile square
   */
  def TileToWGS(tileX: Integer, tileY: Integer, zoom: Integer) : (Double, Double) = {    
    val tx : Double = tileX * 256;
    val ty : Double = tileY * 256;
    
    val mapSize : Double = 256 << zoom;
    val x : Double = (tx / mapSize) - 0.5.toDouble;
    val y : Double = 0.5.toDouble - (ty/ mapSize);
    
    val latitude : Double = 90.toDouble - ((360.toDouble * Math.atan(Math.exp(-y * 2 * Math.PI))) / Math.PI);
    val longitude : Double = 360.toDouble * x;
    (longitude, latitude)
  }
  
  /**
   * @param file delete subtree of this file recursively
   */
  def deleteRecursively(file: File): Unit = {
    if (file.isDirectory)
      file.listFiles.foreach(deleteRecursively)
    if (file.exists && !file.delete)
      throw new Exception(s"Unable to delete ${file.getAbsolutePath}")
  }

  /*            CUSTOM MAPPERS            */
  
  /**
   * @param row 
   * 	has pickupLatitude  on 10th column</br>
   * 	has pickupLongitued on 11th column</br>
   * 	has pickupDate      on  5th column
   * @return (([time_slide], ([tileX], [tileY])), 1)</br> 
   * 	if coordinates outside of boundary box of newyork ((0, (0,0)), 1) <i>this is done for remove the faulty data.</i>
   */
  def mapTimeTileCount(row: Row) = {
      val format = new java.text.SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss") // inline because SDF is not ThreadSafe
      val pLong = row.getString(10).toDouble
      val pLat  = row.getString(11).toDouble
      if(pLong > maxLong || pLong < minLong || pLat > maxLat || pLat < minLat){
        ((0, (0,0)), 1)
      } else{
        val pDate = format.parse(row.getString(5))
        val c = new Calendar.Builder().setInstant(pDate).build()
        val key = ((c.get(Calendar.DAY_OF_WEEK)*100 + c.get(Calendar.HOUR_OF_DAY)).toInt,  WGSToTile(pLong, pLat, ZOOM_LEVEL))
        (key, 1)
      }
  }

  /**
   * group by time slide id and convert tile coordinate to geographic coordinate
   * 
   * @param line (([time_slide], ([tileX], [tileY])), [count])
   * @return ([time_slide], String[]("{location: new google.maps.LatLng([lat], [long]), weight: [count]}"))
   */
  def mapHeatmapPoint(line: ((Int, (Int, Int)), Int)) = {
     val wgs = TileToWGS(line._1._2._1, line._1._2._2, ZOOM_LEVEL)
     (line._1._1, ListBuffer[String]("{location: new google.maps.LatLng(" + wgs._2.toString + ", " + wgs._1.toString + "), weight: " + line._2.toString + "}"))
  }
  /*------------CUSTOM MAPPERS------------*/

  def main(args: Array[String]) {
    val stime = System.currentTimeMillis();
    // input and output paths should be different on different infrastructure
    var input1: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_1.csv" // input file paths
    var input2: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_2.csv"
    var input3: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_3.csv"
    var input4: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_4.csv"
    var input5: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_5.csv"
    var input6: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_6.csv"
    var input7: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_7.csv"
    var input8: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_8.csv"
    var input9: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_9.csv"
    var input10: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_10.csv"
    var input11: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_11.csv"
    var input12: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/NYCTaxi/trip_data_12.csv"
    var output: String = "/Users/erdemuysal/Documents/DERSLER/CENG595BigData/Project/Datasets/tripAnalyze" // default output folder
    val spark = SparkSession.builder() // creating spark instance
      .appName("sparkAssignment")
      .master("local[*]")
      .getOrCreate()

    val sc = spark.sparkContext // getting context
    
    deleteRecursively(new File(output)) // clear old output data if exist
    
    val input1RDD = spark.read.format("CSV").option("header", "true").load(input1).rdd
    val input2RDD = spark.read.format("CSV").option("header", "true").load(input2).rdd
    val input3RDD = spark.read.format("CSV").option("header", "true").load(input3).rdd
    val input4RDD = spark.read.format("CSV").option("header", "true").load(input4).rdd
    val input5RDD = spark.read.format("CSV").option("header", "true").load(input5).rdd
    val input6RDD = spark.read.format("CSV").option("header", "true").load(input6).rdd
    val input7RDD = spark.read.format("CSV").option("header", "true").load(input7).rdd
    val input8RDD = spark.read.format("CSV").option("header", "true").load(input8).rdd
    val input9RDD = spark.read.format("CSV").option("header", "true").load(input9).rdd
    val input10RDD = spark.read.format("CSV").option("header", "true").load(input10).rdd
    val input11RDD = spark.read.format("CSV").option("header", "true").load(input11).rdd
    val input12RDD = spark.read.format("CSV").option("header", "true").load(input12).rdd
    val combinedRDD = (input1RDD ++ input2RDD ++ input3RDD ++ input4RDD ++ input5RDD ++ input6RDD ++ input7RDD ++ input8RDD ++ input9RDD ++ input10RDD ++ input11RDD ++ input12RDD) // Combine 12 seperate csv files in one RDD instance
    
    val header = sc.parallelize(Array("var dataArray = [];","function initDataArray(){")) // just for using output as javascript function
    val footer = sc.parallelize(Array("}"))
    
    val out = combinedRDD.map(row => mapTimeTileCount(row)) // map each line as time slide of tile squares with 1 for accumulate counts of tile squares
                         .reduceByKey(_ + _) // combine count of tile squares of time slide
                         .map(line => mapHeatmapPoint(line)) // group By time slide id and convert tile coordinate to geographic coordinate
                         .reduceByKey(_ ++= _) // union all tile squares data of time slide to a ListBuffer object
                         .mapValues(v => v.mkString("[", ", ", "]"))  // convert values of each key to a javascript array string
                       //.sortByKey(true, 1) // actually this is not necessary, just for manual reading of output data
                         .map(line => s"dataArray[${line._1}] = new google.maps.MVCArray(${line._2});") // convert each line of data to formatted javascript line as string
    header.union(out).union(footer).repartition(1).saveAsTextFile(output) // add required javascript prefix and postfix to output data
    
    println("\nsaved in " + ((System.currentTimeMillis() - stime) / 1000) + " seconds"); // print total running time in seconds
  }
}
