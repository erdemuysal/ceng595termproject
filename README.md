# Hourly Spatial Analysis Of Taxi Passengers #

Use MapReduce for analysis on NYC Taxi Data using the Apache Spark Framework. And visualize hourly intensity of passenger pickups on Google Maps Heatmap Layer.

###Execution Steps###
* Download and import project to Eclipse with Scala IDE Plugin
	* Instruction can be found on this [document](http://freecontent.manning.com/wp-content/uploads/how-to-start-developing-spark-applications-in-eclipse.pdf)
* Run project as Spark Application
* Change the name of the resulting file to "heatmap.js" after the process is complete
* Move the file in the result folder so that it is in the same folder as heatmap.html
* Open heatmap.html using your preferred web browser

*Erdem UYSAL*

2017